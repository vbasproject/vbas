# VBAS : Visual Bulletin Analytics System

An example screenshoot of the VBAS system: 
![alt tag](https://raw.githubusercontent.com/saifulkhan/saifulkhan.github.io/master/data/seismological_data_visualization/vbas-screen.png)


How to compile the code:

1. $ edit_blocks 
Saiful               block:    1 p : 4548 events 2014-03-01 to 2014-03-31 : Worked from            to            : Finished    0 events

2. export BLOCKID=1

3. saiful@sunfire:~> /usr/local/netbeans-8.2/bin/netbeans --jdkhome /usr/lib64/jvm/java-1.8.0-openjdk-1.8.0/

4. Open the vbas project
